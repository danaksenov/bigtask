package com.company;

import java.util.Currency;

public class Generator {


    private Generator(){
    }

    public static Organizations[] generate(){
        Organizations[] organizations = new Organizations[2];
        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency();
            currencies[1] = new Currency();
            currencies[2] = new Currency();
        organizations[0] = new Exchanger("Обменка", "ул. Героев Труда, 10");
        }

        {
            Currency[] currencies = new Currency[2];
            currencies[0] = new Currency();
            currencies[1] = new Currency();
            organizations[1] = new CreditCafe("КредитКафе", "ул. Свободы, 53", 1222);
        }
        return organizations;


    }
}
