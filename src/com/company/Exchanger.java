package com.company;

import java.util.Scanner;

public class Exchanger extends Organizations {
    public float currencyUsd = 27.2f;
    public float currencyEur = 31.5f;

    public Exchanger(String name, String adress) {
        super(name, adress);
    }

    public double changeInUsd(int value){
    return value / currencyUsd;
}

    public double changeInEur(int value){
        return value / currencyEur;
    }

    public float changeUsd(int value) {

        return value * currencyUsd;
    }

    public float changeEur(int value) {

        return value * currencyEur;

    }
    public static void exchange(){
    Scanner scan = new Scanner(System.in);
    String eur = "Eur";
    String usd = "Usd";
    String uah = "Гривну";
    String valuta = "Валюта";

        System.out.println("Поменять валюту или гривну? ");
    String action = scan.next();

        System.out.println("Введите сумму которую хотите поменять: ");
    int value = scan.nextInt();

    Exchanger exchanger = new Exchanger();

        if(action.equalsIgnoreCase(uah)){
        System.out.println("Ваши гривны в евро: " + exchanger.changeInEur(value));
        System.out.println("Ваши гривны в долларах: " + exchanger.changeInUsd(value));
    } else {
            System.out.println("Ваши евро в гривнах: " + exchanger.changeEur(value));
            System.out.println("Ваши доллары в гривнах: " + exchanger.changeUsd(value));

        }
    }

}


